package BalancedArrays;

import java.util.ArrayList;
import java.util.Arrays;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

public class BAAlgorithmWithVectorRotation {

 public static void main(String[] args) {
  //step 1
  SpecialVector v1, v2;
  v1 = new SpecialVector(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 
                                   1, 2, 2, 2, 2, 2, 2, 2, 2});
  v2 = new SpecialVector(new int[]{0, 0, 0, 1, 1, 2, 2, 2, 0, 0, 1, 2, 
                                   2, 0, 0, 0, 1, 1, 2, 2, 2});
  int startFrom = 0;
  //step 2
  SpecialVectorSet r1 = new SpecialVectorSet();
  ICombinatoricsVector<Integer> iv1, iv2, iv3;
  iv1 = Factory.createVector(new Integer[]{1, 1, -1, -1, -1, -1, -1, -1});
  iv2 = Factory.createVector(new Integer[]{1, -1, -1, -1, -1});
  iv3 = Factory.createVector(new Integer[]{1, 1, -1, -1, -1, -1, -1, -1});
  Generator<Integer> generator1 = Factory.createPermutationGenerator(iv1);
  Generator<Integer> generator2 = Factory.createPermutationGenerator(iv2);
  Generator<Integer> generator3 = Factory.createPermutationGenerator(iv3);
  for (ICombinatoricsVector<Integer> p1 : generator1) {
   for (ICombinatoricsVector<Integer> p2 : generator2) {
    for (ICombinatoricsVector<Integer> p3 : generator3) {
     SpecialVector v;
     v = new SpecialVector(p1.getVector(),p2.getVector(),p3.getVector());
     r1.add(v);
    }
   }
  }
  //step 3
  SpecialVectorSet r2 = new SpecialVectorSet();
  for (SpecialVector pat : r1.allvectors) {
   if (r1.isValidAcePatternVector(v2, pat)) {
    r2.add(pat);
   }
  }

  //step 4
  SpecialVectorSet s1 = new SpecialVectorSet();
  for (SpecialVector pat1 : r2.allvectors) {
   s1 = new SpecialVectorSet();
   for (SpecialVector pat2 : r2.allvectors) {
    if (r1.isValidAcePatterns(pat1, pat2)) {
     s1.add(pat2);
    }
   }
   //step 5
   if (s1.allvectors.size() <= 6) {
    continue;
   }

   //step 6
   SpecialVectorSet s = new SpecialVectorSet();
   for (SpecialVector pat3 : s1.allvectors) {
    s = new SpecialVectorSet();
    s.add(pat3);
    for (SpecialVector pat4 : s1.allvectors) {
     if (r1.isValidAcePatterns(pat4, s.allvectors)) {
      s.add(pat4);
     }
    }
    //step 7
    if (s.allvectors.size() <= 7) {
     continue;
    }
    //step 8
    int i = 0;
    SpecialVectorSet[] tees = new SpecialVectorSet[s.allvectors.size()];
    ArrayList<SpecialVectorSet> t1 = new ArrayList<SpecialVectorSet>();

    for (SpecialVector pattern : s.allvectors) {
     SpecialVectorSet tee = createTee(pattern);
     tees[i] = tee;
     t1.add(tee);
     i++;
    }
    //step 9
    findMaximumSets(s, tees, t1, v1, v2);

    System.out.println("Done");

   }
  }

 }

 public static void findMaximumSets(SpecialVectorSet s, 
         SpecialVectorSet[] tees,ArrayList<SpecialVectorSet> t1, 
         SpecialVector v1, SpecialVector v2) {
  SpecialVectorSet svs = new SpecialVectorSet();
  svs.add(v1);
  svs.add(v2);
  for (SpecialVector sv1 : tees[0].allvectors) {
   if (s.isValid(sv1, svs.allvectors)) {
    svs.add(sv1);
    int swaps1 = 0;
    while (svs.allvectors.size() < 4 && swaps1 < 7) {
     for (SpecialVector sv2 : t1.get(1).allvectors) {
      if (s.isValid(sv2, svs.allvectors)) {
       svs.add(sv2);
       int swaps2 = 0;
       while (svs.allvectors.size() < 5 && swaps2 < 6) {
        for (SpecialVector sv3 : t1.get(2).allvectors) {
         if (s.isValid(sv3, svs.allvectors)) {
          svs.add(sv3);
          int swaps3 = 0;
          while (svs.allvectors.size() < 6 && swaps3 < 5) {
           for (SpecialVector sv4 : t1.get(3).allvectors) {
            if (s.isValid(sv4, svs.allvectors)) {
             svs.add(sv4);
             int swaps4 = 0;
             while (svs.allvectors.size() < 7 && swaps4 < 4) {
              for (SpecialVector sv5 : t1.get(4).allvectors) {
               if (s.isValid(sv5, svs.allvectors)) {
                svs.add(sv5);
                System.out.println("7:" + svs.allvectors);
               } else {
                continue;
               }
              }
              SpecialVectorSet badset = t1.remove(4);
              t1.add(badset);
              swaps4++;
             }
             if (svs.allvectors.size() > 9) {
              System.out.println(svs);
             } else {
              svs.allvectors.remove(5);
             }
            } else {
             continue;
            }

           }
           SpecialVectorSet badset = t1.remove(3);
           t1.add(badset);
           swaps3++;

          }
          if (svs.allvectors.size() > 9) {
           System.out.println(svs);
          } else {
           svs.allvectors.remove(4);
          }
         } else {
          continue;
         }
        }
        SpecialVectorSet badset = t1.remove(2);
        t1.add(badset);
        swaps2++;
       }
       if (svs.allvectors.size() > 9) {
        System.out.println(svs);
       } else {
        svs.allvectors.remove(3);
       }
      } else {
       continue;
      }
     }
     SpecialVectorSet badset = t1.remove(1);
     t1.add(badset);
     swaps1++;
    }
    if (svs.allvectors.size() > 9) {
     System.out.println(svs.allvectors);
    } else {
     svs.allvectors.remove(2);
    }
   } else {
    continue;
   }
  }
 }

 public static SpecialVectorSet createTee(SpecialVector seedPattern) {
  SpecialVectorSet tee = new SpecialVectorSet();
  ICombinatoricsVector<Integer> iv1, iv2, iv3;
  iv1 = Factory.createVector(new Integer[]{0, 0, 0, 2, 2, 2});
  iv2 = Factory.createVector(new Integer[]{0, 0, 2, 2});
  iv3 = Factory.createVector(new Integer[]{0, 0, 0, 2, 2, 2});
  Generator<Integer> generator1 = Factory.createPermutationGenerator(iv1);
  Generator<Integer> generator2 = Factory.createPermutationGenerator(iv2);
  Generator<Integer> generator3 = Factory.createPermutationGenerator(iv3);
  for (ICombinatoricsVector<Integer> perm1 : generator1) {
   for (ICombinatoricsVector<Integer> perm2 : generator2) {
    for (ICombinatoricsVector<Integer> perm3 : generator3) {
     int[] values = new int[16];
     int i = 0;
     for (int val : perm1.getVector()) {
      values[i++] = val;
     }
     for (int val : perm2.getVector()) {
      values[i++] = val;
     }
     for (int val : perm3.getVector()) {
      values[i++] = val;
     }
     int[] allvalues = Arrays.copyOf(seedPattern.values, 21);
     int k = 0;
     for (int j = 0; j < 21; j++) {
      if (allvalues[j] == -1) {
       allvalues[j] = values[k];
       k++;
      }
     }
     SpecialVector v = new SpecialVector(allvalues);
     tee.add(v);
    }
   }
  }

  return tee;
 }
}
