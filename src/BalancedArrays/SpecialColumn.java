package BalancedArrays;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

public class SpecialColumn implements Serializable {

    List<Integer> values = new ArrayList<>();

    public SpecialColumn(int[] values) {
        for (int i : values) {
            this.values.add(i);
        }
    }

    public SpecialColumn(List<Integer> values) {
        this.values = values;
    }

    public SpecialColumn(List<Integer> v1, List<Integer> v2, List<Integer> v3) {
        this.values.addAll(v1);
        this.values.addAll(v2);
        this.values.addAll(v3);
    }

    public boolean satisfies(SpecialColumnArray L0) {
        for (int i = 0; i < 4; i++) {
            SpecialColumn c = L0.allcolumns.get(i);
            int c00 = 0, c10 = 0, c20 = 0;
            for (int j = 0; j < 21; j++) {
                if (c.values.get(j) == 0 && this.values.get(j) == 0) {
                    c00++;
                } else if (c.values.get(j) == 1 && this.values.get(j) == 0) {
                    c10++;
                } else if (c.values.get(j) == 2 && this.values.get(j) == 0) {
                    c20++;
                }
            }
//            if (i>=3)
//                System.out.println(i+"="+c00+":"+c10+":"+c20);
            if (c00 != 3 || c10 != 2 || c20 != 2) {
                return false;
            }
        }
        for (int i = 4; i < 7; i++) {
            SpecialColumn c = L0.allcolumns.get(i);
            int c00 = 0, c10 = 0, c20 = 0;
            for (int j = 0; j < 21; j++) {
                if (c.values.get(j) == 0 && this.values.get(j) == 0) {
                    c00++;
                } else if (c.values.get(j) == 1 && this.values.get(j) == 0) {
                    c10++;
                } else if (c.values.get(j) == 2 && this.values.get(j) == 0) {
                    c20++;
                }
            }
//            if (i>=6){
//                System.out.println(i+"=="+c00+":"+c10+":"+c20);
//                System.out.println(c);
//                System.out.println(this);
//            }
            if (c00 != 3 || c10 != 2 || c20 != 2) {
                return false;
            }
        }
        System.out.println("found");
        System.out.println(this);

        return true;
    }

    public boolean step4Satisfies(SpecialColumnArray L0) {
        int thisMatchesLeft = 0;
        for (int i = 0; i < 4; i++) {
            SpecialColumn left = L0.allcolumns.get(i);
            int leftMatchesRight = 0;
            for (int k = 4; k < 7; k++) {
                SpecialColumn right = L0.allcolumns.get(k);
                int c00 = 0, c10 = 0, c20 = 0;
                for (int j = 0; j < 21; j++) {
                    if (left.values.get(j) == 0 && right.values.get(j) == 0) {
                        c00++;
                    } else if (left.values.get(j) == 1 && right.values.get(j) == 0) {
                        c10++;
                    } else if (left.values.get(j) == 2 && right.values.get(j) == 0) {
                        c20++;
                    }
                }
                if (c00 == 2 && c10 == 2 && c20 == 2) {
                    leftMatchesRight++;
//                    break;
                }
            }
            if (leftMatchesRight == 3) {
                int m00 = 0, m10 = 0, m20 = 0;
                for (int j = 0; j < 21; j++) {
                    if (left.values.get(j) == 0 && this.values.get(j) == 0) {
                        m00++;
                    } else if (left.values.get(j) == 1 && this.values.get(j) == 0) {
                        m10++;
                    } else if (left.values.get(j) == 2 && this.values.get(j) == 0) {
                        m20++;
                    }
                }
                if (m00 == 3 && m10 == 2 && m20 == 2) {
//                    System.out.println("found");
//                    System.out.println(this);
                    thisMatchesLeft++;
                }
            } else {
                continue;
            }
        }
        if (thisMatchesLeft == 4) {
            return true;
        } else {
            return false;
        }
    }

    public boolean step6Satisfies(SpecialColumnArray L0) {
        int thisMatchesLeft = 0;
        for (int i = 0; i < 4; i++) {
            SpecialColumn left = L0.allcolumns.get(i);
            int leftMatchesRight = 0;
            for (int k = 4; k < 7; k++) {
                SpecialColumn right = L0.allcolumns.get(k);
                int c12 = 0, c01 = 0, c21 = 0, c22 = 0, c02 = 0, c11 = 0;
                for (int j = 0; j < 21; j++) {
                    if (left.values.get(j) == 1 && right.values.get(j) == 2) {
                        c12++;
                    } else if (left.values.get(j) == 0 && right.values.get(j) == 1) {
                        c01++;
                    } else if (left.values.get(j) == 2 && right.values.get(j) == 1) {
                        c21++;
                    } else if (left.values.get(j) == 2 && right.values.get(j) == 2) {
                        c22++;
                    } else if (left.values.get(j) == 0 && right.values.get(j) == 2) {
                        c02++;
                    } else if (left.values.get(j) == 1 && right.values.get(j) == 1) {
                        c11++;
                    }
                }
                if (c12 == 2 && c01 == 3 && c21 == 3 && c22 == 2 && c02 == 2 && c11 == 3) {
                    leftMatchesRight++;
//                    break;
                }
            }
            if (leftMatchesRight == 3) {
                int m01 = 0, m12 = 0, m21 = 0, m22 = 0, m02 = 0, m11 = 0;
                for (int j = 0; j < 21; j++) {
                    if (left.values.get(j) == 0 && this.values.get(j) == 1) {
                        m01++;
                    } else if (left.values.get(j) == 1 && this.values.get(j) == 2) {
                        m12++;
                    } else if (left.values.get(j) == 2 && this.values.get(j) == 1) {
                        m21++;
                    } else if (left.values.get(j) == 2 && this.values.get(j) == 2) {
                        m22++;
                    } else if (left.values.get(j) == 0 && this.values.get(j) == 2) {
                        m02++;
                    } else if (left.values.get(j) == 1 && this.values.get(j) == 1) {
                        m11++;
                    }
                }
                if (m01 == 2 && m12 == 2 && m21 == 2 && m22 == 3 && m02 == 2 && m11 == 3) {
//                    System.out.println("found");
//                    System.out.println(this);
                    thisMatchesLeft++;
                }
            } else {
                continue;
            }
        }
        if (thisMatchesLeft == 4) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<SpecialColumn> step5Replace() {
        ArrayList<SpecialColumn> generatives = new ArrayList<SpecialColumn>();
        ArrayList<Integer> vec = new ArrayList<Integer>();
        vec.addAll(this.values);
//        System.out.println("*" + this);
        Integer[] acePositions = new Integer[14];
        int i = 0;
        for (int pos = 0; pos < vec.size(); pos++) {
            if (vec.get(pos) == 1) {
                acePositions[i] = pos;
                i++;
            }

        }
        Integer[] part1 = Arrays.copyOfRange(acePositions, 0, 4);
        Integer[] part2 = Arrays.copyOfRange(acePositions, 4, 9);
        Integer[] part3 = Arrays.copyOfRange(acePositions, 9, 14);

        ICombinatoricsVector<Integer> iv1, iv2, iv3;
        iv1 = Factory.createVector(part1);
        iv2 = Factory.createVector(part2);
        iv3 = Factory.createVector(part3);
        Generator<Integer> generator1 = Factory.createSubSetGenerator(iv1);
        Generator<Integer> generator2 = Factory.createSubSetGenerator(iv2);
        Generator<Integer> generator3 = Factory.createSubSetGenerator(iv3);
        for (ICombinatoricsVector<Integer> p1 : generator1) {
            if (p1.getSize() != 2) {
                continue;
            }
            for (ICombinatoricsVector<Integer> p2 : generator2) {
                if (p2.getSize() != 2) {
                    continue;
                }
                for (ICombinatoricsVector<Integer> p3 : generator3) {
                    if (p3.getSize() != 3) {
                        continue;
                    }
//                    System.out.println(p1 + ":" + p2 + ":" + p3);
                    ArrayList<Integer> newvec = new ArrayList<Integer>();
                    newvec.addAll(this.values);
                    newvec.set(p1.getVector().get(0), 2);
                    newvec.set(p1.getVector().get(1), 2);
                    newvec.set(p2.getVector().get(0), 2);
                    newvec.set(p2.getVector().get(1), 2);
                    newvec.set(p3.getVector().get(0), 2);
                    newvec.set(p3.getVector().get(1), 2);
                    newvec.set(p3.getVector().get(2), 2);
                    SpecialColumn v = new SpecialColumn(newvec);
                    generatives.add(v);
//                    System.out.println(" " + v);
                }
            }
        }
//        System.out.println("combs step 5:" + generatives.size());
        return generatives;
    }

    public String toString() {
        String s = "[";
        for (int v : values) {
            s += v + ",";
        }
        return s + "]";
    }

    public String toTabbedString() {
        String s = "";
        for (int v : values) {
            if (v < 0) {
                s += "0\t";
            } else {
                s += v + "\t";
            }
        }
        return s;
    }

    @Override
    public boolean equals(Object o) {
        SpecialColumn sv = (SpecialColumn) o;
        for (int i = 0; i < this.values.size(); i++) {
            if (sv.values.get(i) != this.values.get(i)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.values.hashCode();
        return hash;
    }

    public SpecialColumn(String commaSeparatedLine) {
        String[] tok = commaSeparatedLine.substring(1, commaSeparatedLine.length() - 1).split(",");
        for (String s : tok) {
            this.values.add(new Integer(s));
        }
    }

    public boolean step3_17_2_Satisfies(SpecialColumnArray L0) {
        int thisMatchesLeft = 0;
        for (int i = 0; i < 7; i++) {
            SpecialColumn left = L0.allcolumns.get(i);
            int c01 = 0, c21 = 0, c11 = 0;
            for (int j = 0; j < 21; j++) {
                if (left.values.get(j) == 0 && this.values.get(j) == 1) {
                    c01++;
                } else if (left.values.get(j) == 2 && this.values.get(j) == 1) {
                    c21++;
                } else if (left.values.get(j) == 1 && this.values.get(j) == 1) {
                    c11++;
                }
            }
            if (c01 == 3 && c21 == 3 && c11 == 3) {
                thisMatchesLeft++;
//                    break;
            } else {
                break;
//                return false;
            }
        }
        if (thisMatchesLeft == 7) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<SpecialColumn> step4_17_2_Replace() {
        ArrayList<SpecialColumn> generatives = new ArrayList<SpecialColumn>();
        ArrayList<Integer> vec = new ArrayList<Integer>();
        vec.addAll(this.values);
//        System.out.println("*" + this);
        Integer[] zeroPositions = new Integer[14];
        int i = 0;
        for (int pos = 0; pos < vec.size(); pos++) {
            if (vec.get(pos) == 0) {
                zeroPositions[i] = pos;
                i++;
            }

        }
        Integer[] part1 = Arrays.copyOfRange(zeroPositions, 0, 4);
        Integer[] part2 = Arrays.copyOfRange(zeroPositions, 4, 8);
        Integer[] part3 = Arrays.copyOfRange(zeroPositions, 8, 12);

        ICombinatoricsVector<Integer> iv1, iv2, iv3;
        iv1 = Factory.createVector(part1);
        iv2 = Factory.createVector(part2);
        iv3 = Factory.createVector(part3);
        Generator<Integer> generator1 = Factory.createSubSetGenerator(iv1);
        Generator<Integer> generator2 = Factory.createSubSetGenerator(iv2);
        Generator<Integer> generator3 = Factory.createSubSetGenerator(iv3);
        for (ICombinatoricsVector<Integer> p1 : generator1) {
            if (p1.getSize() != 2) {
                continue;
            }
            for (ICombinatoricsVector<Integer> p2 : generator2) {
                if (p2.getSize() != 2) {
                    continue;
                }
                for (ICombinatoricsVector<Integer> p3 : generator3) {
                    if (p3.getSize() != 2) {
                        continue;
                    }
//                    System.out.println(p1 + ":" + p2 + ":" + p3);
                    ArrayList<Integer> newvec = new ArrayList<Integer>();
                    newvec.addAll(this.values);
                    newvec.set(p1.getVector().get(0), 2);
                    newvec.set(p1.getVector().get(1), 2);
                    newvec.set(p2.getVector().get(0), 2);
                    newvec.set(p2.getVector().get(1), 2);
                    newvec.set(p3.getVector().get(0), 2);
                    newvec.set(p3.getVector().get(1), 2);
                    SpecialColumn v = new SpecialColumn(newvec);
                    generatives.add(v);
//                    System.out.println(" " + v);
                }
            }
        }
//        System.out.println("combs step 5:" + generatives.size());
        return generatives;
    }

    public boolean step5_17_2_Satisfies(SpecialColumnArray L0) {
        int thisMatchesLeft = 0;
        for (int i = 0; i < 7; i++) {
            SpecialColumn left = L0.allcolumns.get(i);
            int c10 = 0, c12 = 0, c00 = 0, c22 = 0, c02 = 0, c20 = 0;
            for (int j = 0; j < 21; j++) {
                if (left.values.get(j) == 1 && this.values.get(j) == 0) {
                    c10++;
                } else if (left.values.get(j) == 1 && this.values.get(j) == 2) {
                    c12++;
                } else if (left.values.get(j) == 0 && this.values.get(j) == 0) {
                    c00++;
                } else if (left.values.get(j) == 2 && this.values.get(j) == 2) {
                    c22++;
                } else if (left.values.get(j) == 0 && this.values.get(j) == 2) {
                    c02++;
                } else if (left.values.get(j) == 2 && this.values.get(j) == 0) {
                    c20++;
                }
            }
            if (c10 == 2 && c12 == 2 && c00 == 2 && c22 == 2 && c02 == 2 && c20 == 2) {
                thisMatchesLeft++;
                if (thisMatchesLeft >= 6) {
                    System.out.println(thisMatchesLeft);
                }
//                    break;
            } else {
//                break;
                // return false;
            }
        }
        if (thisMatchesLeft >= 6) {
            return true;
        } else {
            return false;
        }
    }

    public boolean step6_17_2_Satisfies(SpecialColumn left) {

        int c01 = 0, c10 = 0, c12 = 0, c21 = 0, c00 = 0, c22 = 0, c02 = 0, c20 = 0, c11 = 0;
        for (int j = 0; j < 21; j++) {
            if (left.values.get(j) == 0 && this.values.get(j) == 1) {
                c01++;
            } else if (left.values.get(j) == 1 && this.values.get(j) == 0) {
                c10++;
            } else if (left.values.get(j) == 1 && this.values.get(j) == 2) {
                c12++;
            } else if (left.values.get(j) == 2 && this.values.get(j) == 1) {
                c21++;
            } else if (left.values.get(j) == 0 && this.values.get(j) == 0) {
                c00++;
            } else if (left.values.get(j) == 2 && this.values.get(j) == 2) {
                c22++;
            } else if (left.values.get(j) == 0 && this.values.get(j) == 2) {
                c02++;
            } else if (left.values.get(j) == 2 && this.values.get(j) == 0) {
                c20++;
            } else if (left.values.get(j) == 1 && this.values.get(j) == 1) {
                c11++;
            }
        }
        if (c01 == 2 && c10 == 2 && c12 == 2 && c21 == 2 && c00 == 2 && c22 == 2 && c02 == 2 && c20 == 2 && c11 == 5) {
            return true;
//                    break;
        } else {
            return false;
        }

    }

}
