/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BalancedArrays;

import java.util.Random;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

/**
 *
 * @author herc
 */
public class PBAlgorithm {

    public static void main(String args[]) {
        //step 2 create all combinations of T1
        SpecialColumnArray T1 = new SpecialColumnArray();
        ICombinatoricsVector<Integer> iv1, iv2, iv3;
        iv1 = Factory.createVector(new Integer[]{0, 0, 0, 0, 1, 1, 1});
        iv2 = Factory.createVector(new Integer[]{0, 0, 0, 0, 1, 1, 1});
        iv3 = Factory.createVector(new Integer[]{0, 0, 0, 0, 1, 1, 1});
        Generator<Integer> generator1 = Factory.createPermutationGenerator(iv1);
        Generator<Integer> generator2 = Factory.createPermutationGenerator(iv2);
        Generator<Integer> generator3 = Factory.createPermutationGenerator(iv3);
        for (ICombinatoricsVector<Integer> p1 : generator1) {
            for (ICombinatoricsVector<Integer> p2 : generator2) {
                for (ICombinatoricsVector<Integer> p3 : generator3) {
                    SpecialColumn v = new SpecialColumn(p1.getVector(), p2.getVector(), p3.getVector());
                    T1.add(v);
                }
            }
        }
        System.out.println(T1.allcolumns.size() + " columns in T1");
//        System.out.println(S0.allcolumns.get(r.nextInt(S0.allcolumns.size())));

        for (int i = 0; i < 6; i++) {
            for (int j = 7; j < 13; j++) {
                for (int k = 14; k < 21; k++) {
                    SpecialColumnArray nonIso = getArrayL1();
                    embedRows17_2(nonIso, i, j, k);
                    System.out.println(nonIso);
                    //step 3
                    SpecialColumnArray T11 = new SpecialColumnArray();
                    for (SpecialColumn s : T1.allcolumns) {
                        if (s.step3_17_2_Satisfies(nonIso)) {
                            T11.add(s);
                        }
                    }
                    if (T11.allcolumns.size() > 0) {
                        System.out.println(T11.allcolumns.size() + " columns in T11");
                    }
//                    for (SpecialColumn s : S01.allcolumns) {
//                        System.out.println(s);
//                    }
                    //step 4
                    SpecialColumnArray T112 = new SpecialColumnArray();
                    for (SpecialColumn s : T11.allcolumns) {
                        T112.allcolumns.addAll(s.step4_17_2_Replace());
                    }
                    System.out.println("T112 has:" + T112.allcolumns.size());
                    //step 6
                    SpecialColumnArray T1123 = new SpecialColumnArray();
                    for (SpecialColumn s : T112.allcolumns) {
                        if (s.step5_17_2_Satisfies(nonIso)) {
                            T1123.allcolumns.add(s);
                        }
                    }
                    for (SpecialColumn s : T1123.allcolumns) {
                             System.out.println(s);
                    }

                    System.out.println("********************");
                    System.out.println("T1123 has:" + T1123.allcolumns.size());
                    for (int t = 0; t < T1123.allcolumns.size() - 1; t++) {
                        SpecialColumn left = T1123.allcolumns.get(t);
                        for (int p = t + 1; p < T1123.allcolumns.size(); p++) {
                            SpecialColumn right = T1123.allcolumns.get(p);
                            if (right.step6_17_2_Satisfies(left)) {
                                System.out.println("**FOUND A PAIR*********");

                                System.out.println(left);
                                System.out.println(right);
                                break;
                            }
                        }
                    }
                    System.out.println("********************");

                }
            }
        }

    }

    public static void algo17_1(String args[]) {

        SpecialColumnArray S0 = new SpecialColumnArray();
        ICombinatoricsVector<Integer> iv1, iv2, iv3;
        iv1 = Factory.createVector(new Integer[]{0, 0, 0, 1, 1, 1, 1});
        iv2 = Factory.createVector(new Integer[]{0, 0, 1, 1, 1, 1, 1});
        iv3 = Factory.createVector(new Integer[]{0, 0, 1, 1, 1, 1, 1});
        Generator<Integer> generator1 = Factory.createPermutationGenerator(iv1);
        Generator<Integer> generator2 = Factory.createPermutationGenerator(iv2);
        Generator<Integer> generator3 = Factory.createPermutationGenerator(iv3);
        for (ICombinatoricsVector<Integer> p1 : generator1) {
            for (ICombinatoricsVector<Integer> p2 : generator2) {
                for (ICombinatoricsVector<Integer> p3 : generator3) {
                    SpecialColumn v = new SpecialColumn(p1.getVector(), p2.getVector(), p3.getVector());
                    S0.add(v);
                }
            }
        }
        System.out.println(S0.allcolumns.size() + " columns in S1");
//        System.out.println(S0.allcolumns.get(r.nextInt(S0.allcolumns.size())));

        for (int i = 0; i < 6; i++) {
            for (int j = 7; j < 13; j++) {
                for (int k = 14; k < 21; k++) {
                    SpecialColumnArray nonIso = getArrayL1();
                    embedRows17_1(nonIso, i, j, k);
                    System.out.println(nonIso);
                    //step 4
                    SpecialColumnArray S01 = new SpecialColumnArray();
                    for (SpecialColumn s : S0.allcolumns) {
                        if (s.step4Satisfies(nonIso)) {
                            S01.add(s);
                        }
                    }
                    if (S01.allcolumns.size() > 0) {
                        System.out.println(S01.allcolumns.size() + " columns in S01");
                    }
//                    for (SpecialColumn s : S01.allcolumns) {
//                        System.out.println(s);
//                    }
                    //step 5
                    SpecialColumnArray S012 = new SpecialColumnArray();
                    for (SpecialColumn s : S01.allcolumns) {
                        S012.allcolumns.addAll(s.step5Replace());
                    }
                    System.out.println("S012 has:" + S012.allcolumns.size());
                    //step 6
                    SpecialColumnArray S0121 = new SpecialColumnArray();
                    for (SpecialColumn s : S012.allcolumns) {
                        if (s.step6Satisfies(nonIso)) {
                            S0121.allcolumns.add(s);
                        }
                    }
                    System.out.println("********************");
                    System.out.println("S0121 has:" + S0121.allcolumns.size());
                    for (SpecialColumn s : S0121.allcolumns) {
                        System.out.println(s);
                    }
                    System.out.println("********************");

                }
            }
        }

    }

    public static SpecialColumnArray getArrayL1() {
        SpecialColumnArray nonIsoL1 = new SpecialColumnArray();
        nonIsoL1.add(new SpecialColumn(new int[]{0, 0, 0, 0, 0, 0, 1, 1, 1, 1,
            1, 1, 2, 2, 2, 2, 2, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 2, 0, 1, 2, 0, 1, 2, 0,
            1, 2, 0, 1, 2, 0, 1, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 1, 2, 2, 0, 1, 1, 2, 2,
            0, 0, 1, 2, 1, 0, 0, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 1, 2, 2, 0, 0, 0, 1, 1,
            2, 2, 2, 0, 2, 1, 1, 0}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 2, 1, 0, 2, 1, 0, 0, 2,
            2, 1, 2, 2, 0, 0, 1, 1}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 0, 1, 1, 2, 2, 2, 1, 2, 0,
            1, 0, 2, 0, 0, 1, 2, 1}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 0, 1, 2, 1, 2, 1, 2, 0, 2,
            0, 1, 0, 1, 2, 1, 2, 0}));
        return nonIsoL1;

    }

    public static SpecialColumnArray getArrayL2() {
        SpecialColumnArray nonIsoL1 = new SpecialColumnArray();
        nonIsoL1.add(new SpecialColumn(new int[]{0, 0, 0, 0, 0, 0, 1, 1, 1, 1,
            1, 1, 2, 2, 2, 2, 2, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 2, 0, 1, 2, 0, 1, 2, 0,
            1, 2, 0, 1, 2, 0, 1, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 1, 2, 2, 0, 1, 1, 2, 2,
            0, 0, 1, 2, 1, 0, 0, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 1, 2, 2, 0, 0, 0, 1, 1,
            2, 2, 2, 0, 2, 1, 1, 0}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 2, 1, 0, 2, 1, 0, 0, 2,
            2, 1, 2, 2, 0, 0, 1, 1}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 0, 1, 1, 2, 2, 2, 1, 2, 0,
            0, 1, 2, 1, 0, 1, 2, 0}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 0, 1, 2, 1, 2, 1, 2, 0, 2,
            1, 0, 0, 0, 2, 1, 2, 1}));
        return nonIsoL1;

    }

    public static SpecialColumnArray getArrayL3() {
        SpecialColumnArray nonIsoL1 = new SpecialColumnArray();
        nonIsoL1.add(new SpecialColumn(new int[]{0, 0, 0, 0, 0, 0, 1, 1, 1, 1,
            1, 1, 2, 2, 2, 2, 2, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 2, 0, 1, 2, 0, 1, 2, 0,
            1, 2, 0, 1, 2, 0, 1, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 1, 2, 2, 0, 1, 1, 2, 2,
            0, 0, 1, 2, 1, 0, 0, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 1, 2, 2, 0, 0, 0, 1, 1,
            2, 2, 2, 0, 2, 1, 1, 0}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 0, 1, 2, 2, 1, 2, 0, 2,
            0, 1, 0, 0, 2, 2, 1, 1}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 2, 2, 0, 1, 0, 2, 0, 1,
            1, 2, 1, 2, 0, 2, 0, 1}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 0, 1, 2, 1, 2, 1, 2, 2, 0,
            1, 0, 2, 0, 0, 1, 2, 1}));
        return nonIsoL1;

    }

    public static SpecialColumnArray getArrayL0() {
        SpecialColumnArray nonIsoL1 = new SpecialColumnArray();
        nonIsoL1.add(new SpecialColumn(new int[]{0, 0, 0, 0, 0, 0, 1, 1, 1, 1,
            1, 1, 2, 2, 2, 2, 2, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 2, 0, 1, 2, 0, 1, 2, 0,
            1, 2, 0, 1, 2, 0, 1, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 1, 2, 2, 0, 1, 1, 2, 2,
            0, 0, 1, 2, 1, 0, 0, 2}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 1, 2, 2, 0, 0, 0, 1, 1,
            2, 2, 2, 0, 2, 1, 1, 0}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 0, 1, 2, 2, 1, 2, 0, 2,
            0, 1, 0, 0, 2, 2, 1, 1}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 2, 2, 0, 1, 0, 2, 0, 1,
            1, 2, 1, 2, 0, 2, 0, 1}));
        nonIsoL1.add(new SpecialColumn(new int[]{0, 1, 0, 0, 1, 0, 2, 1, 2, 2,
            1, 0, 2, 0, 0, 2, 1, 1}));
        return nonIsoL1;

    }

    public static void embedRows17_1(SpecialColumnArray nonIsoL1, int pos0, int pos1, int pos2) {

        nonIsoL1.allcolumns.get(0).values.add(pos0, 0);
        nonIsoL1.allcolumns.get(1).values.add(pos0, 0);
        nonIsoL1.allcolumns.get(2).values.add(pos0, 0);
        nonIsoL1.allcolumns.get(3).values.add(pos0, 0);
        nonIsoL1.allcolumns.get(4).values.add(pos0, 1);
        nonIsoL1.allcolumns.get(5).values.add(pos0, 1);
        nonIsoL1.allcolumns.get(6).values.add(pos0, 1);

        nonIsoL1.allcolumns.get(0).values.add(pos1, 1);
        nonIsoL1.allcolumns.get(1).values.add(pos1, 1);
        nonIsoL1.allcolumns.get(2).values.add(pos1, 1);
        nonIsoL1.allcolumns.get(3).values.add(pos1, 1);
        nonIsoL1.allcolumns.get(4).values.add(pos1, 1);
        nonIsoL1.allcolumns.get(5).values.add(pos1, 1);
        nonIsoL1.allcolumns.get(6).values.add(pos1, 1);

        nonIsoL1.allcolumns.get(0).values.add(pos2, 2);
        nonIsoL1.allcolumns.get(1).values.add(pos2, 2);
        nonIsoL1.allcolumns.get(2).values.add(pos2, 2);
        nonIsoL1.allcolumns.get(3).values.add(pos2, 2);
        nonIsoL1.allcolumns.get(4).values.add(pos2, 1);
        nonIsoL1.allcolumns.get(5).values.add(pos2, 1);
        nonIsoL1.allcolumns.get(6).values.add(pos2, 1);

    }

    public static void embedRows17_2(SpecialColumnArray nonIsoL1, int pos0, int pos1, int pos2) {
        for (int i = 0; i < 7; i++) {
            nonIsoL1.allcolumns.get(i).values.add(pos0, 0);
            nonIsoL1.allcolumns.get(i).values.add(pos1, 1);
            nonIsoL1.allcolumns.get(i).values.add(pos2, 2);
        }

    }
}
