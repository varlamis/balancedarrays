package BalancedArrays;

import java.util.ArrayList;
import java.util.HashSet;

public class SpecialColumnArray {

    ArrayList<SpecialColumn> allcolumns;

    public SpecialColumnArray() {
        allcolumns = new ArrayList<SpecialColumn>();
    }

    public void add(SpecialColumn column) {
        allcolumns.add(column);
    }
/*
    public boolean isValid(SpecialVector s1, ArrayList<SpecialVector> allvs) {
        for (SpecialVector s2 : allvs) {
            if (!isValid(s1, s2)) {
                return false;
            }
        }
        return true;
    }

    public boolean isValid(SpecialVector s1, SpecialVector s2) {
        if (s1.equals(s2)) {
            return true;
        }
        Counters c = new Counters();
        for (int i = 0; i < 21; i++) {
            if (s1.values[i] == 0 && s2.values[i] == 0) {
                c.n00++;
                if (c.n00 > 3) {
                    return false;
                }
            } else if (s1.values[i] == 0 && s2.values[i] == 1) {
                c.n01++;
                if (c.n01 > 2) {
                    return false;
                }
            } else if (s1.values[i] == 0 && s2.values[i] == 2) {
                c.n02++;
                if (c.n02 > 3) {
                    return false;
                }
            } else if (s1.values[i] == 1 && s2.values[i] == 0) {
                c.n10++;
                if (c.n10 > 2) {
                    return false;
                }
            } else if (s1.values[i] == 1 && s2.values[i] == 1) {
                c.n11++;
                if (c.n11 > 1) {
                    return false;
                }
            } else if (s1.values[i] == 1 && s2.values[i] == 2) {
                c.n12++;
                if (c.n12 > 2) {
                    return false;
                }
            } else if (s1.values[i] == 2 && s2.values[i] == 0) {
                c.n20++;
                if (c.n20 > 3) {
                    return false;
                }
            } else if (s1.values[i] == 2 && s2.values[i] == 1) {
                c.n21++;
                if (c.n21 > 2) {
                    return false;
                }
            } else if (s1.values[i] == 2 && s2.values[i] == 2) {
                c.n22++;
                if (c.n22 > 3) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isValidAcePatternVector(SpecialVector s2, SpecialVector pattern) {
        if (pattern.equals(s2)) {
            return true;
        }
        Counters c = new Counters();
        for (int i = 0; i < 21; i++) {
            if (pattern.values[i] == -1) {
                continue;
            } else if (s2.values[i] == 0 && pattern.values[i] == 1) {
                c.n01++;
                if (c.n01 > 2) {
                    return false;
                }
            } else if (s2.values[i] == 2 && pattern.values[i] == 1) {
                c.n21++;
                if (c.n21 > 2) {
                    return false;
                }
            } else if (s2.values[i] == 1 && pattern.values[i] == 1) {
                c.n11++;
                if (c.n11 > 1) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isValidAcePatterns(SpecialVector pattern1, SpecialVector pattern2) {
        if (pattern1.equals(pattern2)) {
            return true;
        }
        Counters c = new Counters();
        for (int i = 0; i < 21; i++) {
            if (pattern1.values[i] == 1 && pattern2.values[i] == 1) {
                c.n11++;
                if (c.n11 > 1) {
                    return false;
                }
            }
        }
        if (c.n11 < 1) {
            return false;
        }
        return true;
    }

    public boolean isValidAcePatterns(SpecialVector s1, ArrayList<SpecialVector> allvs) {
        for (SpecialVector s2 : allvs) {
            if (!isValidAcePatterns(s1, s2)) {
                return false;
            }
        }
        return true;
    }
    */
    public String toString(){
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<this.allcolumns.size();i++){
            SpecialColumn sc=this.allcolumns.get(i);
            sb.append("[");
            for (int j=0;j<sc.values.size();j++)
                sb.append(sc.values.get(j)+",");
            sb.append("]\n");
        }
        return sb.toString();
    }
}
