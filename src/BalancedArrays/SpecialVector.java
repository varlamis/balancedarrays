package BalancedArrays;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;

public class SpecialVector implements Serializable {

    int[] values = new int[21];
    int c0 = 0, c1 = 0, c2 = 0;

    public SpecialVector(int[] values) {
        this.values = values;
    }

    public SpecialVector(List<Integer> values) {
        this.values = ArrayUtils.toPrimitive(values.toArray(new Integer[0]));
    }

    public SpecialVector(List<Integer> v1, List<Integer> v2, List<Integer> v3) {
        ArrayList<Integer> all = new ArrayList<>();
        all.addAll(v1);
        all.addAll(v2);
        all.addAll(v3);
        this.values = ArrayUtils.toPrimitive(all.toArray(new Integer[0]));
    }

    public boolean isValid() {
        for (int x : values) {
            if (x == 0) {
                c0++;
                if (c0 > 8) {
                    return false;
                }
            } else if (x == 1) {
                c1++;
                if (c1 > 5) {
                    return false;
                }
            } else {
                c2++;
                if (c2 > 8) {
                    return false;
                }
            }
        }
        return true;
    }

    public String toString() {
        String s = "[";
        for (int v : values) {
            s += v + ",";
        }
        return s + "]";
    }

    public String toTabbedString() {
        String s = "";
        for (int v : values) {
            if (v < 0) {
                s += "0\t";
            } else {
                s += v + "\t";
            }
        }
        return s;
    }

    @Override
    public boolean equals(Object o) {
        SpecialVector sv = (SpecialVector) o;
        if (Arrays.equals(this.values, sv.values)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Arrays.hashCode(this.values);
        return hash;
    }

    public SpecialVector(String commaSeparatedLine) {
        String[] tok = commaSeparatedLine.substring(1, commaSeparatedLine.length() - 1).split(",");
        int i = 0;
        for (String s : tok) {
            this.values[i] = new Integer(s).intValue();
            i++;
        }
    }
}
